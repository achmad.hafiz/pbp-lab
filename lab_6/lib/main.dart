import 'package:flutter/material.dart';
import 'package:lab_6/screens/home.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Daftar Karantina',
      theme: ThemeData(
          primarySwatch: Colors.blueGrey,

          // Define the default font family.

          // Define the default `TextTheme`. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          textTheme: TextTheme(
            headline6:
                GoogleFonts.lato(fontSize: 30.0, fontStyle: FontStyle.normal),
            bodyText2: GoogleFonts.lato(
                fontSize: 16), // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
          )),
      home: const MyHomePage(title: 'Daftar Karantina'),
    );
  }
}
