import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _name = "";
  String? _lokasi = "Pilih Lokasi";
  int _counter = 0;
  String? _pickedDate = "Pilih Tanggal Karantina";

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  List<String> _listLokasi = [
    "RSUD Kanujoso Djatiwibowo",
    "RSPB",
    "RS Restu Ibu",
    "RSJ"
  ];

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
        ),
        body: Container(
          margin: const EdgeInsets.only(left: 100, right: 100),
          child: Center(
              // Center is a layout widget. It takes a single child and positions it
              // in the middle of the parent.
              child: Column(
            // Column is also a layout widget. It takes a list of children and
            // arranges them vertically. By default, it sizes itself to fit its
            // children horizontally, and tries to be as tall as its parent.
            //
            // Invoke "debug painting" (press "p" in the console, choose the
            // "Toggle Debug Paint" action from the Flutter Inspector in Android
            // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
            // to see the wireframe for each widget.
            //
            // Column has various properties to control how it sizes itself and
            // how it positions its children. Here we use mainAxisAlignment to
            // center the children vertically; the main axis here is the vertical
            // axis because Columns are vertical (the cross axis would be
            // horizontal).
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(
                      child: Text(
                        "Nama",
                        textAlign: TextAlign.center,
                      ),
                      width: 80,
                    ),
                    Container(
                      child: const Icon(Icons.person),
                      margin: const EdgeInsets.only(right: 20),
                    ),
                    Flexible(
                        child: TextFormField(
                            decoration: const InputDecoration(
                      hintText: 'Masukkan Nama',
                    )))
                  ]),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(
                    width: 80,
                    child: Text(
                      "Lokasi",
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    child: const Icon(Icons.local_hospital),
                    margin: const EdgeInsets.only(right: 20),
                  ),
                  Flexible(
                      child: DropdownButtonFormField<String>(
                    items: _listLokasi.map((lokasiValue) {
                      return DropdownMenuItem<String>(
                        child: Text(lokasiValue),
                        value: lokasiValue,
                      );
                    }).toList(),
                    decoration: InputDecoration(
                      hintText: _lokasi,
                    ),
                    onChanged: (String? lokasiValue) {
                      setState(() {
                        _lokasi = lokasiValue;
                      });
                    },
                  ))
                ],
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(
                        width: 80,
                        child: Text(
                          "Tanggal",
                          textAlign: TextAlign.center,
                        )),
                    Container(
                      child: const Icon(Icons.calendar_today),
                      margin: const EdgeInsets.only(right: 20),
                    ),
                    Flexible(
                      child: TextFormField(
                        decoration: InputDecoration(
                          hintText: _pickedDate,
                        ),
                        onTap: () async {
                          _pickedDate = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime.now(),
                            lastDate: DateTime(2101),
                          ).toString();
                        },
                      ),
                    )
                  ]),
              Container(
                  padding: const EdgeInsets.only(left: 150.0, top: 40.0),
                  width: 300,
                  height: 80,
                  child: ElevatedButton(
                    child: Text('Submit'),
                    onPressed: null,
                  )),
            ],
          )),
          // This trailing comma makes auto-formatting nicer for build methods.
        ));
  }
}
