from django.db import models

class Note(models.Model):
    to_who = models.CharField(max_length=20)
    from_who = models.CharField(max_length=20)
    title = models.CharField(max_length=20)
    message = models.TextField(max_length=300)

# Create your models here.
