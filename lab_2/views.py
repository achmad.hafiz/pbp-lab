from django.http import response
from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from .models import Note


def index(request):
    messages = Note.objects.all()
    response = {'messages' : messages}
    return render(request, 'lab_2.html', response)

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")

# Create your views here.
