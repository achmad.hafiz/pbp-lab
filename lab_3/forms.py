from django.forms import ModelForm
from django.db.models import fields
from django.forms import widgets
from lab_1.models import Friend

class DateInput(widgets.DateInput):
    input_type = 'date'


class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
        widgets = {'date_of_birth' : DateInput()}
    

