
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required


@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all()
    response = {'friends' : friends}
    return render(request, 'friend_list_lab3.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    context = {}
    form = FriendForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3/')
    context['form'] = form
    return render(request, 'lab3_form.html', context)
# Create your views here.
