from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

def index(request):
    messages = Note.objects.all()
    response = {"messages": messages}
    return render(request, 'lab4_index.html', response)
def add_note(request):
    context = {}
    form = NoteForm(request.POST)
    if request.method == "POST" and form.is_valid:
        form.save()
        return HttpResponseRedirect('/lab-4/')
    context["form"] = form
    return render(request, 'lab4_form.html', context)
def note_list(request):
    messages = Note.objects.all()
    response = {"messages": messages}
    return render(request, 'lab4_note_list.html', response)
# Create your views here.
