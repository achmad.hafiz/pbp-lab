import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DaftarKarantinaForm extends StatefulWidget {
  const DaftarKarantinaForm({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<DaftarKarantinaForm> createState() => _DaftarKarantinaFormState();
}

class _DaftarKarantinaFormState extends State<DaftarKarantinaForm> {
  final _formKey = GlobalKey<FormState>();

  String? _name;
  String? _lokasi;
  DateTime? _pickedDate;

  List<String> _listLokasi = [
    "RSUD Kanujoso Djatiwibowo",
    "RSPB",
    "RS Restu Ibu",
    "RSJ"
  ];

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
          centerTitle: true,
        ),
        body: Form(
            key: _formKey,
            child: SingleChildScrollView(
                child: Container(
              padding: const EdgeInsets.only(left: 30, right: 30, top: 20),
              child: Center(
                  // Center is a layout widget. It takes a single child and positions it
                  // in the middle of the parent.
                  child: Column(
                // Column is also a layout widget. It takes a list of children and
                // arranges them vertically. By default, it sizes itself to fit its
                // children horizontally, and tries to be as tall as its parent.
                //
                // Invoke "debug painting" (press "p" in the console, choose the
                // "Toggle Debug Paint" action from the Flutter Inspector in Android
                // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
                // to see the wireframe for each widget.
                //
                // Column has various properties to control how it sizes itself and
                // how it positions its children. Here we use mainAxisAlignment to
                // center the children vertically; the main axis here is the vertical
                // axis because Columns are vertical (the cross axis would be
                // horizontal).
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            const SizedBox(
                              child: Text(
                                "Nama",
                                textAlign: TextAlign.center,
                              ),
                              width: 60,
                            ),
                            Flexible(
                                child: TextFormField(
                              decoration: InputDecoration(
                                  hintText: 'Masukkan Nama',
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  icon: const Icon(Icons.person)),
                              validator: (value) {
                                if (value == null || value.isEmpty)
                                  return "Mohon isi nama Anda!";
                              },
                              onSaved: (value) {
                                setState(() {
                                  _name = value;
                                });
                              },
                            ))
                          ])),
                  Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          const SizedBox(
                            width: 60,
                            child: Text(
                              "Lokasi",
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Flexible(
                              child: DropdownButtonFormField<String>(
                            items: _listLokasi.map((lokasiValue) {
                              return DropdownMenuItem<String>(
                                child: Text(lokasiValue),
                                value: lokasiValue,
                              );
                            }).toList(),
                            decoration: InputDecoration(
                                hintText: "Pilih Lokasi",
                                //_lokasi == null ? "Pilih Lokasi" : _lokasi,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                icon: const Icon(Icons.local_hospital)),
                            onChanged: (lokasiValue) {
                              setState(() {
                                _lokasi = lokasiValue;
                              });
                            },
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Mohon pilih lokasi karantina!";
                              }
                            },
                          ))
                        ],
                      )),
                  Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            const SizedBox(
                                width: 60,
                                child: Text(
                                  "Tanggal",
                                  textAlign: TextAlign.center,
                                )),
                            Flexible(
                              child: TextFormField(
                                decoration: InputDecoration(
                                    hintText: _pickedDate != null
                                        ? _pickedDate
                                            ?.toIso8601String()
                                            .substring(0, 10)
                                        : "Pilih Tanggal Karantina",
                                    icon: const Icon(Icons.calendar_today),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0))),
                                onTap: () async {
                                  await showDatePicker(
                                    context: context,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime.now(),
                                    lastDate: DateTime(2101),
                                  ).then((value) {
                                    setState(() {
                                      _pickedDate = value;
                                    });
                                  });
                                },
                              ),
                            )
                          ])),
                  Container(
                      padding: const EdgeInsets.only(left: 150.0, top: 40.0),
                      width: 300,
                      height: 80,
                      child: ElevatedButton(
                        child: const Text('Submit'),
                        onPressed: () {
                          if (!_formKey.currentState!.validate()) {
                            return;
                          }
                          _formKey.currentState!.save();
                        },
                      )),
                ],
              )),
              // This trailing comma makes auto-formatting nicer for build methods.
            ))));
  }
}
